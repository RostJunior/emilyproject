# Hello! #

#This is my first project online store in Java (Hibernate, JavaMail, Spring, MultipartFile, JavaIO, MySql, Servlets, JSP, XML, HTML, CSS, JavaScript, JQuery, Bootstrap, Spring Security)#

#Online shop made on the basis of the website (http://emily.hol.es)[emily.hol.es] to which was attached: #
* 1 - registration and authorization of users; 
* 2 - feedback form with the administration; 
* 3 - admin panel; 
* 4 - cart; 
* 5 - possibility of ordering and sending of mail order.... 

#Admin panel: #
* 1 - managing products (adding, editing, selection by type type control...); 
* 2 - delivery management; 
* 3 - user management; 
* 4 - order management, reports....; 
######[for the database](https://github.com/RostJunior/sql2/blob/master/emily2.sql)


## Code: ##
[### emilyproject ###](https://bitbucket.org/RostJunior/emilyproject/src)